﻿using laba_webnet_1;
using MyDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace laba_webnet_1.Tests
{
    public class CircularLinkedListTests
    {
        [Fact]
        public void EventDetailsTest()
        {
            Assert.True(false);
        }

        [Fact]
        public void EventDetailsTest1()
        {
            Assert.False(true);
        }

        [Theory]
        [InlineData(new int[4] { -1, 0, 30000, 4 })]
        public void CreateIntLinkedList_ReturnTrueIfOk(int[] values)
        {
            var testList = new CircularLinkedList<int>("test");
            foreach (var value in values)
            {
                testList.Add(value);
            }

            Assert.Equal(testList, values);
        }
    }
}