﻿using MyDLL;
using System;
using System.Collections;
using System.Collections.Generic;

namespace laba_webnet_1
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                string type = "";
                bool isOk = true;
                try
                {
                    Console.WriteLine("CircularLinkedList.\nChoose type:" +
                        "\n1. int" +
                        "\n2. string");
                    int operation = Convert.ToInt32(Console.ReadLine());
                    switch (operation)
                    {
                        case 1:
                            type = "int";
                            CircularLinkedList<int> list = new CircularLinkedList<int>("список 1. ");
                            list.Added += (stack, eventResult) => Console.WriteLine(((CircularLinkedList<int>)stack).name+ eventResult.message+ " " + eventResult.value);
                            list.Removed += (stack, eventResult) => Console.WriteLine(((CircularLinkedList<int>)stack).name + eventResult.message + " " + eventResult.value);
                            list.Cleared += (stack, eventResult) => Console.WriteLine(((CircularLinkedList<int>)stack).name+eventResult.message + " " + eventResult.value);

                            Console.ReadKey();
                            do
                            {
                                Console.Clear();
                                try
                                {
                                    int opt = ChooseOperation(list);
                                    switch (opt)
                                    {
                                        case 1: Console.Write("Value to add: ");
                                            list.Add(Convert.ToInt32(Console.ReadLine()));
                                            break;
                                        case 2:
                                            Console.Write("Value to remove: ");
                                            list.Remove(Convert.ToInt32(Console.ReadLine()));
                                            break;
                                        case 3:
                                            Console.WriteLine("Are you sure you want to clear? (y/n)");
                                            if (Console.ReadLine() == "y")
                                                list.Clear();
                                            break;
                                        case 4:
                                            Console.Write("Value to check inside: ");
                                            list.Contains(Convert.ToInt32(Console.ReadLine()));
                                            break;
                                        case 5:
                                            isOk = false;
                                            break;
                                    }
                                }
                                catch (NotFoundNodeDataException e)
                                {
                                    Console.Write(e.Message);
                                }
                                catch (EmptyListException e)
                                {
                                    Console.Write(e.Message);
                                }
                                catch (FormatException e)
                                {
                                    Console.Write("Wrong input! Or incorrect data type. Your type is "+type);
                                }
                                catch
                                {
                                    Console.Write("Oops");
                                }
                                Console.ReadKey();
                                
                                
                            } while (isOk);
                            break;

                        case 2:
                            type = "string";
                            ICircularLinkedList<string> stringlist = new CircularLinkedList<string>("name1");
                            Console.ReadKey();
                            do
                            {
                                Console.Clear();
                                try
                                {
                                    int opt = ChooseOperation(stringlist);
                                    switch (opt)
                                    {
                                        case 1:
                                            Console.Write("Value to add: ");
                                            stringlist.Add(Console.ReadLine());
                                            break;
                                        case 2:
                                            Console.Write("Value to remove: ");
                                            stringlist.Remove(Console.ReadLine());
                                            break;
                                        case 3:
                                            Console.WriteLine("Are you sure you want to clear? (y/n)");
                                            if (Console.ReadLine() == "y")
                                                stringlist.Clear();
                                            break;
                                        case 4:
                                            Console.Write("Value to check inside: ");
                                            stringlist.Contains(Console.ReadLine());
                                            break;
                                        case 5:
                                            isOk = false;
                                            break;
                                    }
                                }
                                catch (NotFoundNodeDataException e)
                                {
                                    Console.Write(e.Message);
                                }
                                catch (EmptyListException e)
                                {
                                    Console.Write(e.Message);
                                }
                                catch (FormatException e)
                                {
                                    Console.Write("Wrong input! Or incorrect data type. Your type is " + type);
                                }
                                catch
                                {
                                    Console.Write("Oops");
                                }
                                Console.ReadKey();


                            } while (isOk);
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Wrong type. Please, try again.");
                }
                Console.ReadKey();

            } while (true);


        }
        public static void DisplayList<T>(ICircularLinkedList<T> circularList)
        {
            Console.Write("The list: ");
                foreach (var item in circularList)
                {
                    Console.Write(item+", ");
                }
            Console.WriteLine();
        }
        public static int ChooseOperation<T>(ICircularLinkedList<T> circularList)
        {
            DisplayList(circularList);
            Console.WriteLine("Choose operation:\n1. Add;\n2. Remove;\n3. Clear;\n4. Contains;\n5. Go back.");
            return Convert.ToInt32(Console.ReadLine());
        }
        
    }
}
