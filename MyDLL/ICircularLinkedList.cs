﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDLL
{
    public interface ICircularLinkedList<T>: IEnumerable<T>
    {
        public void Add(T data);
        public bool Remove(T data);
        public void Clear();
        public bool Contains(T data);
    }
}
