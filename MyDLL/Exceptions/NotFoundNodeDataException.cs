﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDLL
{
    public class NotFoundNodeDataException : Exception
    {
        public NotFoundNodeDataException() { }
        public NotFoundNodeDataException(string name)
            : base(String.Format("Not found such node data in list: {0}", name))
        {
        }
    }
}
