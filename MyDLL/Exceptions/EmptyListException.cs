﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDLL
{
    public class EmptyListException : Exception
    {
        public EmptyListException() : base(String.Format("The list is empty!")) { }
    }
}
