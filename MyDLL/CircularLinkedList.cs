﻿using MyDLL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba_webnet_1
{
    public class EventDetails<T>
    {
        public string message { get; private set; }
        public T value { get; private set; }
        public IEnumerable<T> list { get; private set; }
        public EventDetails(string message, T v)
        {
            this.message = message;
            this.value = v;
        }
        public EventDetails(string message, IEnumerable<T> list)
        {
            this.message = message;
            this.list = list;
        }
        
        public EventDetails(string message, IEnumerable<T> list, T v)
        {
            this.message = message;
            this.list = list;
            this.value = v;
        }
    }

    public class CircularLinkedList<T> : ICircularLinkedList<T>
    {
        public delegate void CircularListDelegate(object sender, EventDetails<T> args);

        public event CircularListDelegate SimpleNotify;
        public event CircularListDelegate Added;
        public event CircularListDelegate Removed;
        public event CircularListDelegate Cleared;

        public string name { get; set; } = "unknown";
        Node<T> head; // головной/первый элемент
        Node<T> tail; // последний/хвостовой элемент
        int count;  // количество элементов в списке

        public CircularLinkedList(string name){
            this.name = name;
}

        // добавление элемента
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            // если список пуст
            if (head == null)
            {
                head = node;
                tail = node;
                tail.Next = head;
            }
            else
            {//указатель ставим между  концом--><--началом
                node.Next = head; // <--ставим указатель для элемента, которого добавляем, что следующая - голова
                tail.Next = node; // --> ставим указатель, что после нынешнего конца есть ещё элемент - тот, который добавляем
                tail = node; // присваиваем хвост этому элементу, что добавляется
            }
            count++;

            Added?.Invoke(this, new EventDetails<T>("New node was added. ", node.Data));
        }
        public bool Remove(T data)
        {
            Node<T> current = head;
            Node<T> previous = null;

            if (IsEmpty) throw new EmptyListException();

            do // пробегаемся по всем элементам списка и находим вершину
            {
                if (current.Data.Equals(data)) // когда нашли
                {
                    // Если узел в середине или в конце
                    if (previous != null)
                    {
                        // убираем узел current, теперь previous ссылается не на current, а на current.Next
                        previous.Next = current.Next;

                        // Если узел последний,
                        // изменяем переменную tail
                        if (current == tail)
                            tail = previous;
                    }
                    else // если удаляется первый элемент
                    {

                        // если в списке всего один элемент
                        if (count == 1)
                        {
                            head = tail = null;
                        }
                        else
                        {
                            head = current.Next;
                            tail.Next = current.Next;
                        }
                    }
                    count--;

                    Removed?.Invoke(this, new EventDetails<T>("New node was deleted. ", data));

                    return true;
                }

                previous = current;
                current = current.Next;
            } while (current != head);
            SimpleNotify?.Invoke(this, new EventDetails<T>("The node wasn't removed!", data));
            throw new NotFoundNodeDataException(data.ToString());
        }

        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }

        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
            //Notify?.Invoke("Everything was cleared!");
        }

        public bool Contains(T data)
        {
            Node<T> current = head;
            if (current == null) throw new EmptyListException();
            do
            {
                if (current.Data.Equals(data))
                {
                    //Notify?.Invoke("The "+data+" exists in list.");
                    return true;
                }
                    
                current = current.Next;
            }
            while (current != head);
            throw new NotFoundNodeDataException(data.ToString());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }
    }
}
